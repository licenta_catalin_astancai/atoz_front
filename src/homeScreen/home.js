import React from 'react';

import Carousel from 'react-bootstrap/Carousel';
import { Navbar, Nav, NavItem, NavDropdown, FormControl} from 'react-bootstrap';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import image1 from '../commons/images/iphone.jpg';
import image2 from '../commons/images/jordan.jpg';
import NavigationBar from '../navigation-bar';
import FooterTemplate from '../footer';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import Search from "../components/search";
class Home extends React.Component{ 
  
  
  render(){
  return (
        <div >
 
            <header>
            <NavigationBar/>
            
              </header>
        <body>
     
          <Carousel>
                <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={image1}
                  alt="First slide"
                />
                <Carousel.Caption>
                <a href="/smartPhone"><h3>See All</h3></a>
                 
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
             
                <img
                  className="d-block w-100"
                  src={image2}
                  alt="Second slide"
                />
                <Carousel.Caption>
                <a href="/fashion"> <h3>See All</h3></a>
                 
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
          </body>

          <div class="fixed-bottom">
               
               <FooterTemplate />
              
           </div>
       
       
   
         

         
  </div>
  );
  }
}

 

export default Home;
