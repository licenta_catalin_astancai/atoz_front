import axios from "axios";
import {HOST} from'../../commons/host';
import authHeader from './auth-header';


  const  getAllCustomers=() => {
      return axios.get(HOST.backend_api + '/customer', { headers: authHeader() });
    };
  
  const  getAllUsers=() => {
      return axios.get(HOST.backend_api + 'users', { headers: authHeader() });
    };
  
    
  
  
  export default {
    getAllCustomers,
    getAllUsers
  };