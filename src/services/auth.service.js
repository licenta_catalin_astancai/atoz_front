import axios from "axios";
import {HOST} from'../commons/host';


  const  login=(username, password)=> {
        return axios
          .post(HOST.backend_api + "/api/auth/signin", { username, password })
          .then((response) => {
            if (response.data.token) {
              localStorage.setItem("user", JSON.stringify(response.data));
              localStorage.setItem("token", response.data.token);
            }
    
            return response.data;
          });
      };
 const     logout=()=> {
        localStorage.removeItem("user");
      };
  const    register=(username, password, nume, telefon, adresa)=> {
        return axios.post(HOST.backend_api + "/api/auth/signup", {
          username,
          password,
          nume,
          telefon,
          adresa
        });
      };
    const   getCurrentUser =() => {
        return JSON.parse(localStorage.getItem("user"));
      };
      

export default{
    register,
    login,
    getCurrentUser,
    logout

};