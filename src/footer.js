import React from 'react';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";


const FooterTemplate = () =>(
 
    <MDBFooter color="stylish-color-dark" className="page-footer font-small pt-4 mt-4">
    <MDBContainer fluid className="text-center text-md-left">
      <MDBRow>
        <MDBCol md="6">
         
        </MDBCol>
        <hr className="clearfix w-100 d-md-none" />
        <MDBCol md="2">
          <h5 className="text-uppercase mb-4 mt-3 font-weight-bold">
            Links
          </h5>
          <ul className="list-unstyled">
            <li>
              <a href="/fashion">Fashion Articles</a>
            </li>
            <li>
              <a href="/smartPhone">Smart Phones</a>
            </li>
         
          </ul>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
    <hr />
    <div className="text-center py-0">
      <ul className="list-unstyled list-inline mb-0">
        <li className="list-inline-item">
          <h5 className="mb-0">Register for free</h5>
        </li>
        <li className="list-inline-item">
          <a href="/register" className="btn btn-danger btn-rounded">
            Sign up!
          </a>
        </li>
      </ul>
    </div>
    <hr />
   
    <div className="footer-copyright text-center py-3">
      <MDBContainer fluid>
        &copy; {new Date().getFullYear()} Copyright: <a href="https://www.http://localhost:3000/"> AtoZ.com </a>
      </MDBContainer>
    </div>
  </MDBFooter>

);

export default FooterTemplate;