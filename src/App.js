import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from "./homeScreen/home";
import Fashion from "./components/fashion/fashionItem";
import FashionItemDisplay from "./components/fashion/fashionItemDisplay";
import SmartPhoneItems from "./components/electronics/smartPhoneItem";
import SmartPhoneDisplay from "./components/electronics/smartPhoneDisplay";
import Register from './components/register';
import Login from './components/login';
import Profile from './components/profile';
import DisplaySearchProduct from './components/products/displaySearchProduct';
import Cart from './components/cart';
import Admin from './components/admin/admin';
import AdminFashion from './components/admin/adminFashion';
import AdminSmartPhone from './components/admin/adminSmartPhone';
import FashionInsert from './components/admin/adminFashionOps/adminFashionAdd';
import SmartPhoneInsert from './components/admin/adminSmartPhone/adminSmartPhoneAdd';
import FashionUpdate from './components/admin/adminFashionOps/adminFashionUpdate';
import Statistic from './components/statistics/statictis';
import TurnoverMonth from './components/statistics/turnoverMonth';
class App extends React.Component {

    

    render() {
        

        return (

            <Router>


                <Switch>
                    <Route
                        exact
                        path='/'
                        render={() => <Home />}
                    />

                    <Route
                        exact
                        path='/register'
                        render={() => <Register />}
                    />
                    <Route
                        exact
                        path='/login'
                        
                        render={()=><Login/>}
                    />
                    
                    <Route
                        exact
                        path='/profile'
                       
                        render={()=><Profile/>}
                    />

                    <Route
                        exact
                        path='/fashion'
                        component={Fashion}
                    // render={() => <Fashion/>}
                    />

                    <Route
                        exact
                        path='/fashion/:id'
                        component={FashionItemDisplay}
                        render={() => <FashionItemDisplay />}
                    />


                    <Route
                        exact
                        path='/products/:id'
                        component={DisplaySearchProduct}
                        render={() => <DisplaySearchProduct />}
                    />

                    <Route
                        exact
                        path='/smartPhone'
                        component={SmartPhoneItems}
                    // render={() => <Fashion/>}
                    />

                    <Route
                        exact
                        path='/smartPhone/:id'
                        component={SmartPhoneDisplay}
                        render={() => <SmartPhoneDisplay />}
                    />

                    <Route
                        exact
                        path='/cart'
                        component={Cart}
                        render={() => <Cart />}
                    />

                    <Route
                        exact
                        path='/admin'
                        component={Admin}
                        render={() => <Admin />}
                    />     
                     <Route
                        exact
                        path='/admin/fashion'
                        component={AdminFashion}
                        render={() => <AdminFashion />}
                    />    
                     <Route
                        exact
                        path='/admin/smartphone'
                        component={AdminSmartPhone}
                        render={() => <AdminSmartPhone />}
                    />   
                      <Route
                        exact
                        path='/admin/smartPhone/insert'
                        component={SmartPhoneInsert}
                        render={() => <SmartPhoneInsert />}
                    />     
                    <Route
                        exact
                        path='/admin/fashion/insert/'
                        component={FashionInsert}
                        render={() => <FashionInsert />}
                    />
                    <Route
                        exact
                        path='/admin/fashion/update/:id'
                        component={FashionUpdate}
                        render={() => <FashionUpdate />}
                    />   
                    <Route
                        exact
                        path='/admin/statistic'
                        component={Statistic}
                        render={() => <Statistic />}
                    />
                     <Route
                        exact
                        path='/admin/turnover'
                        component={TurnoverMonth}
                        render={() => <TurnoverMonth />}
                    />      


                </Switch>



            </Router>





        );
    }
}



export default App;
