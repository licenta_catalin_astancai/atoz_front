import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import { Navbar, Nav, NavItem, NavDropdown, FormControl } from 'react-bootstrap';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Login from "./components/login";
import Register from "./components/register";

import Profile from "./components/profile";
import AuthService from "./services/auth.service";
import Search from "./components/search";
const NavigationBar = () => {

  const [currentUser, setCurrentUser] = useState(undefined);
  

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    
    if (user) {
      setCurrentUser(user);
      
      console.log(user.role);

    }
  }, []);



  const logOut = () => {
    AuthService.logout();
  };
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand class="nav-brand" href="/">A to Z</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">



            <NavDropdown title="Products" id="basic-nav-dropdown">
              <NavDropdown.Item href="/fashion"> <i>Fashion</i></NavDropdown.Item>
              <NavDropdown.Item href="/smartPhone"><i>SmartPhones</i></NavDropdown.Item>

            

            </NavDropdown>


          </Nav>
          <Search />


    


          {currentUser ? (
                  <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <Link to={"/profile"} className="nav-link">
                        Hello {currentUser.username}
                      </Link>
                    </li>
                    <li className="nav-item">
                      <a href="/login" className="nav-link" onClick={logOut}>
                        LogOut
              </a>
                    </li>
                  </div>
                ) : (
                  <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <Link to={"/login"} className="nav-link">
                        Login
              </Link>
                    </li>

                    <li className="nav-item">
                      <Link to={"/register"} className="nav-link">
                        Sign Up
              </Link>
                    </li>
                  </div>
                )
                }


       
          
         
        </Navbar.Collapse>

              <a href="/cart"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="black" class="bi bi-cart" viewBox="0 0 16 16" >

                <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
              </svg>
              </a>
      </Navbar>
    </div>
  );
};
export default NavigationBar;
