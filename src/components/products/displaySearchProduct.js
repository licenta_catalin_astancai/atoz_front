
import React from 'react';
import NavigationBar from '../../navigation-bar';
import * as API_PROUCT from './productApi';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import FooterTemplate from '../../footer';
class DisplaySearchProduct extends React.Component {


    state = {
        productItem: null,
        id: null,
        isLoaded: false,
        errorStatus: 0,
        error: null
    };

    async componentDidMount() {
        this.fetchFashionItems();

    }
    fetchFashionItems() {
        let string = String(window.location.pathname.replace('/products/', ''));
        let id_ok = string;
        API_PROUCT.getProductsByName(id_ok, (result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    productItem: result,
                    isLoaded: true
                });
                console.log(this.state.productItem);


            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    displayProduct() {
        if (this.state.productItem != null) {
            return (
                this.state.productItem.map((el, i) => {
                    return (

                        <div class="col-md-2 mt-11">
                            <Card key={i} border="secondary" style={{ width: '18rem' }}>
                                <Card.Img key={i} variant="top" src={`${el.productImage}`} />
                                <Card.Body key={i}>
                                    <Card.Title key={i}>{el.name}</Card.Title>
                                    <Card.Text key={i}>{el.price}</Card.Text>
                                    <Link to={`/${el.category}/${el.id}`} className="btn btn-primary">ViewProduct</Link>

                                </Card.Body>
                            </Card>
                        </div>

                    )
                })

            )
        }
    }

        render() {
            return (

                <div >

                    <header>
                        <NavigationBar />
                    </header>
                    <body>

                        <div class="card-group">

                            {this.displayProduct()}

                        </div>




                    </body>
                    <footer>
                   
                        <FooterTemplate />

                    </footer>
                </div>
            );
        }
    
 } export default DisplaySearchProduct;