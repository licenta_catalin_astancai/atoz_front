import {HOST} from'../../commons/host';
import RestApiClient from "../../commons/api/rest-client.js";
import { MDBCarouselItem } from 'mdbreact';

const endpoint={
    postOrder:"/orderdetails",
    
};


function insertOrder(order, callback) {
    let user = localStorage.getItem('token');
    console.log(JSON.stringify(order));
    let request = new Request(HOST.backend_api + endpoint.postOrder , {
        method: 'POST',
        headers : {
            'Authorization': 'Bearer ' + user,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },

       body: JSON.stringify(order)
    });
    RestApiClient.performRequest(request, callback);
}

export{
    insertOrder
};