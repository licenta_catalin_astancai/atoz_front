import {HOST} from'../../commons/host';
import RestApiClient from "../../commons/api/rest-client.js";

const endpoint={
    get_fashion:"/products"
};

function getProducts(callback){
    let request = new Request(HOST.backend_api + endpoint.get_fashion, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getProductsByName(params,callback){
    let request = new Request(HOST.backend_api + endpoint.get_fashion+"/"+ params, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback); 
}

function getProductsById(params,callback){
    let request = new Request(HOST.backend_api + endpoint.get_fashion+"/findById/"+ params, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback); 
}

export{
    getProducts,
    getProductsByName,
    getProductsById
};