import {HOST} from'./../commons/host';
import RestApiClient from"./../commons/api/rest-client.js";
const endpoint={
    get_customer:"/customer",
    getUsers:"/users"
};

function getUsers(callback){
    let request = new Request(HOST.backend_api + endpoint.getUsers, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCustomer(callback){
    let request = new Request(HOST.backend_api + endpoint.get_customer, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback); 
}

function getCustomerById(params,callback){
    let request = new Request(HOST.backend_api + endpoint.get_customer+"/"+ params, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback); 
}

export{
    getCustomerById,
    getUsers,
    getCustomer
};