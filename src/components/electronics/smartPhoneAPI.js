import {HOST} from'../../commons/host';
import RestApiClient from "../../commons/api/rest-client.js";
const endpoint={
    get_smartPhones:"/smartPhone",
    delte_action:"/delete/"
};


function getSmartPhones(callback){
    let request = new Request(HOST.backend_api + endpoint.get_smartPhones, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getSmartPhoneById(params,callback){
    let request = new Request(HOST.backend_api + endpoint.get_smartPhones+"/"+ params, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback); 
}



function deleteSmartPhone(params, callback){
    let user = localStorage.getItem('token');
    let request = new Request(HOST.backend_api+endpoint.get_smartPhones+ endpoint.delte_action + params, {
        method: 'DELETE',
        headers : {
            'Authorization': 'Bearer ' + user,
          
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function insertProductSmartPhone(product, callback) {
    
    let user = localStorage.getItem('token');
    console.log(JSON.stringify(product));
    let request = new Request(HOST.backend_api + endpoint.get_smartPhones , {
        method: 'POST',
        headers : {
            'Authorization': 'Bearer ' + user,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },

       body: JSON.stringify(product)
    });
    RestApiClient.performRequest(request, callback);
}

export{
    getSmartPhones,
    getSmartPhoneById,
    deleteSmartPhone,
    insertProductSmartPhone
};