import React from 'react';
import NavigationBar from '../../navigation-bar';
import FooterTemplate from '../../footer';
import * as API_FASHION from './smartPhoneAPI';
import { Link } from 'react-router-dom';
class SmartPhoneDisplay extends React.Component {

    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }
    state = {
        smartPhoneItem: null,
        id: null,
        isLoaded: false,
        errorStatus: 0,
        error: null,

        id: null,
        name: null,
        price: null,
        stock: null,
        category: null,
        image: null,
        q: null,
        quantityFinal: ''
    };


    handleChange(e) {
        this.state.quantityFinal = e.currentTarget;
    }

    handleInput(e) {
        this.state.quantityFinal = e.currentTarget;
    }
    async componentDidMount() {
        this.fetchSmartPhoneItem();

    }
    fetchSmartPhoneItem() {
        let string = String(window.location.pathname.replace('/smartPhone/', ''));
        let id_ok = string;
        API_FASHION.getSmartPhoneById(id_ok, (result, status, err) => {
            localStorage.setItem(result, JSON.stringify(result));
            if (result !== null && status === 200) {
                this.setState({
                    smartPhoneItem: result,
                    isLoaded: true
                });
                console.log(this.state.fashionItem);


            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
    addToCart() {

        if (this.state.smartPhoneItem != null) {
            let productCart = {
                id: this.state.smartPhoneItem.id,
                name: this.state.smartPhoneItem.name,
                price: this.state.smartPhoneItem.price,
                category: this.state.smartPhoneItem.category,
                image: this.state.smartPhoneItem.productImage,
                stock: this.state.smartPhoneItem.stock,
                q: Number(this.state.quantityFinal.value)

            };


            alert('Produs adaugat cu succes in cos');
            var existingEntries = JSON.parse(localStorage.getItem("allEntries"));
            if (existingEntries == null) existingEntries = [];

            existingEntries.push(productCart);
            localStorage.setItem("allEntries", JSON.stringify(existingEntries));
        }
    };
    displayProduct() {
        if (this.state.smartPhoneItem != null) {
            return (
                <div class="col-md-11 mt-1">



                    <div class="col-md-7">
                        <p class="newarrival text-center"></p>
                        <h2>{this.state.smartPhoneItem.name}</h2>
                        <p><b>Model {this.state.smartPhoneItem.model}</b></p>
                        <p><b>Display Size: {this.state.smartPhoneItem.dispsize} $</b></p>
                        <p><b>Resolution: {this.state.smartPhoneItem.resolution}</b></p>
                        <p><b>Main Camera:</b>{this.state.smartPhoneItem.maincamera}</p>
                        <p><b>Front Camera:</b>{this.state.smartPhoneItem.frontcamera}</p>
                        <p><b>CPU:</b>{this.state.smartPhoneItem.cpu}</p>
                        <p><b>Memory:</b>{this.state.smartPhoneItem.memory}</p>
                        <p><b>Battery:</b>{this.state.smartPhoneItem.battery}</p>
                        <p><b>Description:</b>{this.state.smartPhoneItem.description}</p>

                    </div>

                </div>





            )
        }
    }

    displayProductImage() {
        if (this.state.smartPhoneItem != null) {
            return (<img src={`${this.state.smartPhoneItem.productImage}`} class="rounded float-right" />
            )
        }
    }


    render() {
        return (

            <div >

                <header>
                    <NavigationBar />
                </header>
                <body>

                    <div class="col-md-10">
                        
                        <form action>
                        {this.displayProduct()}
                            <label class="newarrival text-center" for="quantityFinal"><b></b></label>
                            <input type="number" id="quantityFinal" name="quantityFinal" min="1" max="50"  onChange={this.handleChange} onInput={this.handleInput} />
                            <input type="submit" value="Add to Cart" onClick={this.addToCart} />


                        </form>
                        {this.displayProductImage()}
                    </div>




                </body>

            </div>

        );
    }

}
export default SmartPhoneDisplay;
