import React from 'react';
import './cardStyles.css';

import NavigationBar from '../../navigation-bar';
import FooterTemplate from '../../footer';
import Card from 'react-bootstrap/Card';

import * as API_FASHION from './smartPhoneAPI';
import { Link } from 'react-router-dom';
class SmartPhoneItems extends React.Component {


   

    state = {
        smartPhoneItem: [],
        isLoaded: false,
        errorStatus: 0,
        error: null
    };



    async componentDidMount() {
        this.fetchSmartPhone();
    }


    fetchSmartPhone() {
        API_FASHION.getSmartPhones((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    smartPhoneItem: result,
                    isLoaded: true
                });
                console.log(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    displayCards() {
      
       

        if (this.state.smartPhoneItem != null) {
            return (
                this.state.smartPhoneItem.map((el, i) => {
                    return (
                       
                        <div class="col-md-2 mt-11">
                        <Card key={i} border="secondary" style={{ width: '18rem' }}>
                            <Card.Img key={i} variant="top" src={`${el.productImage}`} />
                            <Card.Body key={i}>
                                <Card.Title key={i}>{el.name}</Card.Title>
                                <Card.Text key={i}>{el.brand}</Card.Text>
                                <Card.Text key={i}>{el.memory}</Card.Text>
                                <Card.Text key={i}>{el.price}</Card.Text>
                              
                                <Link to={`/smartPhone/${el.id}`} className="btn btn-primary">ViewProduct</Link>
                               
                            </Card.Body>
                        </Card>  
                         </div>
                        
                    )
                })

            )
        }
    }


    render() {
        if (this.state.smartPhoneItem.length >=7 ){
        return (

            <div >

                <header>
                    <NavigationBar />
                </header>
                <body>
                
                    <div class="card-group">

                        {this.displayCards()}

                    </div>
                



                </body>
                <footer>
               
                    <FooterTemplate />
                   
                </footer>
            </div>
        );
        }
        else{
            return (

                <div >

                    <header>
                        <NavigationBar />
                    </header>
                    <body>

                        <div class="card-group">

                            {this.displayCards()}

                        </div>




                    </body>
                    <footer>
                        <div class="fixed-bottom">

                            <FooterTemplate />

                        </div>


                    </footer>
                </div>
            );
        }
    }

}
export default SmartPhoneItems;