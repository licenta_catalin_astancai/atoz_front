import React from 'react';
import Swal from 'sweetalert2';
import * as API_SMARTPHONE from '../../electronics/smartPhoneAPI';
import { Navbar, Nav, NavItem, NavDropdown, FormControl } from 'react-bootstrap';
class SmartPhoneInsert extends React.Component {


    constructor(props) {
        super(props);
        this.state = { name: '' };
        this.handleChange = this.handleChange.bind(this);
        this.addFormData = this.addFormData.bind(this);

    }


    state = {
        file: null,
        base64URL: "",
        imagedata: String
    };


    handleChange(file) {

        let idCardBase64 = '';
        idCardBase64 = this.getBase64(file[0]);

    }

    getBase64 = (file) => {
        return new Promise((resolve) => {
            let fileInfo;
            let baseURL = "";
            // Make new FileReader
            let reader = new FileReader();

            // Convert the file to base64 text
            reader.readAsDataURL(file);

            // on reader load somthing...
            reader.onload = () => {
                // Make a fileInfo Object
                console.log("Called", reader);
                baseURL = reader.result;
                this.setState({
                    imagedata: baseURL,
                })
                console.log(baseURL);
                resolve(baseURL);
            };
            console.log(fileInfo);
        });
    };




    registerProductSmartPhone(smartPhoneItem) {
        API_SMARTPHONE.insertProductSmartPhone(smartPhoneItem, (result, status, error) => {

            if (result !== null && (status === 200 || status === 201)) {

                console.log("Successfully inserted order " + result);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        })
    }


    handleName = (e) => {
        this.setState({ name: e.target.value });

    }

    handlePrice = (e) => {
        this.setState({ price: e.target.value });

    }

    handleStock = (e) => {
        this.setState({ stock: e.target.value });

    }

    handleModel = (e) => {
        this.setState({ model: e.target.value });

    }
    handleDispsize = (e) => {
        this.setState({ dispsize: e.target.value });

    }

    handleBrand = (e) => {
        this.setState({ brand: e.target.value });

    }

    handleResolution = (e) => {
        this.setState({ resolution: e.target.value });

    }

    handleMaincamera = (e) => {
        this.setState({ maincamera: e.target.value });

    }

    handleFrontcamera = (e) => {
        this.setState({ frontcamera: e.target.value });

    }
    handleCpu = (e) => {
        this.setState({ cpu: e.target.value });

    }

    handleMemory = (e) => {
        this.setState({ memory: e.target.value });

    }

    handleBattery = (e) => {
        this.setState({ battery: e.target.value });

    }

    handleDescription = (e) => {
        this.setState({ description: e.target.value });

    }



    addFormData(evt) {




        console.log(this.state.imagedata);

        var smartPhoneItem = {}
        smartPhoneItem.name = this.state.name;
        smartPhoneItem.price = this.state.price;
        smartPhoneItem.productImage = this.state.imagedata;
        smartPhoneItem.stock = this.state.stock;
        smartPhoneItem.category = "smartPhone";
        smartPhoneItem.brand = this.state.brand;
        smartPhoneItem.model = this.state.model;
        smartPhoneItem.dispsize = this.state.dispsize;
        smartPhoneItem.resolution = this.state.resolution;
        smartPhoneItem.maincamera = this.state.maincamera;
        smartPhoneItem.frontcamera = this.state.frontcamera;
        smartPhoneItem.cpu = this.state.cpu;
        smartPhoneItem.memory = this.state.memory;
        smartPhoneItem.battery = this.state.battery;
        smartPhoneItem.description = this.state.description;
        console.log(smartPhoneItem);
        this.registerProductSmartPhone(smartPhoneItem);
        Swal.fire({
            title: 'Produsul a fost adaugat cu succes',
            text: "Echipa AtoZ",
            type: 'success',

        });


    }


    render() {

        var user = JSON.parse(localStorage.getItem('user'));
        if(user.role == 'admin'){
        return (


            <div>
               

                    <header>
                        <Navbar bg="light" expand="lg">
                            <Navbar.Brand class="nav-brand" href="/admin">A to Z</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">



                                    <NavDropdown title="Products" id="basic-nav-dropdown">
                                        <NavDropdown.Item href="/admin/fashion"> <i>Fashion</i></NavDropdown.Item>
                                        <NavDropdown.Item href="/admin/smartPhone"><i>SmartPhones</i></NavDropdown.Item>



                                    </NavDropdown>


                                </Nav>











                            </Navbar.Collapse>

                        </Navbar>
                    </header >
                    <div class="container main-container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 className="text-center mt-5 mb-5">AtoZ</h1>
                                <h1 className="text-center mt-5 mb-5">Introduceti datele produsului pe care doriti sa il postati</h1>
                                <form className="mt-5 mb-5">
                                    <div className="form-group">
                                        <input type="text" className="form-control" id="productname" placeholder="Numele Produsului " value={this.state.name} onChange={this.handleName} />
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control" id="price" placeholder="Pretul produsului " value={this.state.price} onChange={this.handlePrice} />
                                    </div>
                                    <div className="form-group">
                                        <label for="image">Product Image:</label>
                                        <input onChange={(e) => this.handleChange(e.target.files)} type="file" className="form-control" id="image" ref="productimage" />
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control" id="stock" placeholder="Stocul initial al produsului " value={this.state.stock} onChange={this.handleStock} />
                                    </div>


                                    <div className="form-group">
                                        <input type="text" className="form-control" id="brand" placeholder="Brandul Produsului" value={this.state.brand} onChange={this.handleBrand} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="model" placeholder="Numele modelului " value={this.state.model} onChange={this.handleModel} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="dispsize" placeholder="Marimea display in inch " value={this.state.dispsize} onChange={this.handleDispsize} />
                                    </div>



                                    <div className="form-group">
                                        <input type="text" className="form-control" id="resolution" placeholder="Rezolutia produsului" value={this.state.resolution} onChange={this.handleResolution} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="maincamera" placeholder="Dimensiunile camerelor principale in MP(ex 35MP+23MP)" value={this.state.maincamera} onChange={this.handleMaincamera} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="frontcamera" placeholder="Dimensiunile camerei frontale in MP(ex 35MP+23MP)" value={this.state.frontcamera} onChange={this.handleFrontcamera} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="cpu" placeholder="Procesorul dispozitivului " value={this.state.cpu} onChange={this.handleCpu} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="memory" placeholder="Memoria dispzozitiv  STOCARE+RAM " value={this.state.memory} onChange={this.handleMemory} />
                                    </div>


                                    <div className="form-group">
                                        <input type="text" className="form-control" id="battery" placeholder="Bateria dispozitivului exprimata in mh" value={this.state.battery} onChange={this.handleBattery} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text-area" rows="5" className="form-control" id="description" placeholder="Descrierea produsului" value={this.state.description} onChange={this.handleDescription} />
                                    </div>

                                    <button type="submit" className="btn btn-primary" onClick={this.addFormData}>Submit</button>
                                </form>








                            </div >
                        </div>
                    </div>
                </div>
                );
            } else {
                return (
                    <h1>ACCES RESTRICTIONAT<a href="/">404 ERROR PAGE</a></h1>
    
                );
            }
    }
} export default SmartPhoneInsert;