import React from 'react';
import { Link } from "react-router-dom";
import { Navbar, Nav, NavItem, NavDropdown, FormControl } from 'react-bootstrap';
import AuthService from "../../services/auth.service";
import * as API_USER from "../profileApi";
import Card from 'react-bootstrap/Card';
class Admin extends React.Component {


    state = {
        userItem: [],
        isLoaded: false,
        errorStatus: 0,
        error: null
    };



    async componentDidMount() {
        this.fetchCustomer();
    }


    fetchCustomer() {
        API_USER.getCustomer((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    userItem: result,

                    isLoaded: true
                });
                console.log(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    displayCards() {



        if (this.state.userItem != null) {
            return (
                this.state.userItem.map((el, i) => {

                    return (

                        <div class="col-md-2 mt-11">
                            <Card key={i} border="secondary" style={{ width: '20rem' }}>

                                <Card.Body key={i}>
                                    <Card.Title key={i}>{el.nume}</Card.Title>
                                   
                                    <Card.Text key={i}>{el.telefon}</Card.Text>
                                    <Card.Text key={i}>{el.adresa}</Card.Text>
                                    <Card.Text key={i}>{el.role}</Card.Text>
                                    <Card.Text key={i}>{el.username}</Card.Text>




                                </Card.Body>
                            </Card>
                        </div>

                    )
                })

            )
        }
    }


    checkAdmin() {
        var user = JSON.parse(localStorage.getItem('user'));
        if (user.role == 'admin') {
            return (
                <div>
                    <header>
                        <Navbar bg="light" expand="lg">
                            <Navbar.Brand class="nav-brand" href="/">A to Z</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">



                                    <NavDropdown title="Products" id="basic-nav-dropdown">
                                        <NavDropdown.Item href="/admin/fashion"> <i>Fashion</i></NavDropdown.Item>
                                        <NavDropdown.Item href="/admin/smartPhone"><i>SmartPhones</i></NavDropdown.Item>



                                    </NavDropdown>


                                </Nav>











                            </Navbar.Collapse>

                        </Navbar>
                    </header >
                    <body>
                        <h1>Lista tuturor entitatilor din baza de date</h1>
                        <div class="card-group">

                            {this.displayCards()}

                        </div>
                    </body>




                </div >
            );

        } else {
            return (
                <h1>ACCES RESTRICTIONAT<a href="/">404 ERROR PAGE</a></h1>

            );
        }
    }

    render() {


        return (

            <div>
                {this.checkAdmin()}

            </div >
        );
    }
}
export default Admin;