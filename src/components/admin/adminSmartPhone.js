import React from 'react';
import { Link } from "react-router-dom";
import { Navbar, Nav, NavItem, NavDropdown, FormControl } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import * as API_SMARTPHONE from '../electronics/smartPhoneAPI';
class AdminSmartPhone extends React.Component {




    state = {
        smartPhoneItem: [],
        isLoaded: false,
        errorStatus: 0,
        error: null
    };



    async componentDidMount() {
        this.fetchSmartPhone();
    }

    fetchSmartPhone() {
        API_SMARTPHONE.getSmartPhones((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    smartPhoneItem: result,
                    isLoaded: true
                });
                console.log(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteSmartPhone(id) {
        console.log("salut " + id);
        return API_SMARTPHONE.deleteSmartPhone(id, (result, status, error) => {
            if (result !== null && status === 200) {
                window.location.reload(false);
                console.log("Successfully deleted patient with id: " + result);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    displayCards() {



        if (this.state.smartPhoneItem != null) {
            return (
                this.state.smartPhoneItem.map((el, i) => {
                    return (

                        <div class="col-md-2 mt-11">
                            <Card key={i} border="secondary" style={{ width: '18rem' }}>
                                <Card.Img key={i} variant="top" src={`${el.productImage}`} />
                                <Card.Body key={i}>
                                    <Card.Title key={i}>{el.name}</Card.Title>
                                    <Card.Text key={i}>Brand:{el.brand}</Card.Text>
                                    <Card.Text key={i}>Memory:{el.memory}</Card.Text>
                                    <Card.Text key={i}>Pret:{el.price}</Card.Text>

                                    <Link to={`/smartPhone/${el.id}`} className="btn btn-primary">ViewProduct</Link>
                                    <Card.Text > </Card.Text>
                                    <button type="submit" className="btn btn-danger" onClick={this.deleteSmartPhone.bind(this, el.id)}>DELETE ITEM</button>

                                </Card.Body>
                            </Card>
                        </div>

                    )
                })

            )
        }
    }
    render() {

        var user = JSON.parse(localStorage.getItem('user'));
        if(user.role == 'admin'){
        return (

            <div>
                <header>
                    <Navbar bg="light" expand="lg">
                        <Navbar.Brand class="nav-brand" href="/admin">A to Z</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">



                                <NavDropdown title="Products" id="basic-nav-dropdown">
                                    <NavDropdown.Item href="/admin/fashion"> <i>Fashion</i></NavDropdown.Item>
                                    <NavDropdown.Item href="/admin/smartPhone"><i>SmartPhones</i></NavDropdown.Item>



                                </NavDropdown>


                            </Nav>











                        </Navbar.Collapse>

                    </Navbar>
                </header >
                <h1>Bun venit la pagina de administrator SmartPhone</h1>
                <div>




                    <h2>Lista tuturor produselor de tip fashion</h2>
                 
                  
                    <h3> </h3>
                    <h3>Operatii disponibile </h3>
                    <Link to={`/admin/smartPhone/insert`} className="btn btn-primary">Add specific SmartPhone Item   </Link>
                  
                    <div class="card-group">

                        {this.displayCards()}

                    </div>





                </div >



            </div >
        );
        }
        else{
            return(
                <h1>ACCES RESTRICTIONAT<a href="/">404 ERROR PAGE</a></h1>
                
            );
        }
    }
} export default AdminSmartPhone;