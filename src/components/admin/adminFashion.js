import React from 'react';
import { Link } from "react-router-dom";
import { Navbar, Nav, NavItem, NavDropdown, FormControl } from 'react-bootstrap';
import * as API_FASHION from '../fashion/fashionApi';

import Card from 'react-bootstrap/Card';
class AdminFashion extends React.Component {


    constructor(props) {
        super(props);





    }
    state = {
        fashionItems: [],
        isLoaded: false,
        errorStatus: 0,
        error: null
    };
    async componentDidMount() {
        this.fetchFashionItems();
    }


    fetchFashionItems() {
        API_FASHION.getFashionProducts((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    fashionItems: result,

                    isLoaded: true
                });
                console.log(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    deleteFashioItem(id) {
        console.log("salut " + id);
        return API_FASHION.deleteFashion(id, (result, status, error) => {
            if (result !== null && status === 200) {
                window.location.reload(false);
                console.log("Successfully deleted patient with id: " + result);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    displayCards() {



        if (this.state.fashionItems != null) {
            return (
                this.state.fashionItems.map((el, i) => {

                    return (
                     

                                <div class="col-md-2 mt-11">
                                    <Card key={i} border="secondary" style={{ width: '18rem' }}>
                                        <Card.Img key={i} variant="top" src={`${el.productImage}`} />
                                        <Card.Body key={i}>
                                            <Card.Title key={i}>Nume: {el.name}</Card.Title>
                                            <Card.Text key={i}>Gen: {el.gender}</Card.Text>
                                            <Card.Text key={i}>Marime: {el.size}</Card.Text>
                                            <Card.Text key={i}>Pret: {el.price}</Card.Text>

                                            <Link to={`/fashion/${el.id}`} className="btn btn-primary">ViewProduct</Link>
                                            <Card.Text > </Card.Text>
                                            <Link to={`/admin/fashion/update/${el.id}`} className="btn btn-primary">UpdateProduct</Link>
                                            <Card.Text > </Card.Text>
                                            <button type="submit" className="btn btn-danger" onClick={this.deleteFashioItem.bind(this, el.id)}>DELETE ITEM</button>
                                        </Card.Body>
                                    </Card>
                                </div>
                       
                    )
                })

            )
        }
    }
    render() {

        var user = JSON.parse(localStorage.getItem('user'));
        if (user.role == 'admin') {
            return (

                <div>
                    <header>
                        <Navbar bg="light" expand="lg">
                            <Navbar.Brand class="nav-brand" href="/admin">A to Z</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">



                                    <NavDropdown title="Products" id="basic-nav-dropdown">
                                        <NavDropdown.Item href="/admin/fashion"> <i>Fashion</i></NavDropdown.Item>
                                        <NavDropdown.Item href="/admin/smartPhone"><i>SmartPhones</i></NavDropdown.Item>



                                    </NavDropdown>


                                </Nav>











                            </Navbar.Collapse>

                        </Navbar>
                    </header >

                    <h1>Bun venit la pagina de administrator Fashion</h1>

                    <h2>Lista tuturor produselor de tip fashion</h2>

                    <h3>Operatii disponibile </h3>
                    <Link to={`/admin/fashion/insert`} className="btn btn-primary">Add specific Fashion Item   </Link>
                    <h3> </h3>
                    <div class="card-group">

                        {this.displayCards()}

                    </div>






                </div >
            );
        }
        else {
            return (
                <h1>ACCES RESTRICTIONAT<a href="/">404 ERROR PAGE</a></h1>

            );
        }
    }
} export default AdminFashion;