import React from 'react';
import Swal from 'sweetalert2';
import * as API_FASHION from '../../fashion/fashionApi';
import { Navbar, Nav, NavItem, NavDropdown, FormControl } from 'react-bootstrap';
class FashionInsert extends React.Component {


    constructor(props) {
        super(props);
        this.state = { name: '' };
        this.handleChange = this.handleChange.bind(this);
        this.addFormData = this.addFormData.bind(this);

    }


    state = {
        file: null,
        base64URL: "",
        imagedata: String
    };


    handleChange(file) {

        let idCardBase64 = '';
        idCardBase64 = this.getBase64(file[0]);

    }

    getBase64 = (file) => {
        return new Promise((resolve) => {
            let fileInfo;
            let baseURL = "";
            // Make new FileReader
            let reader = new FileReader();

            // Convert the file to base64 text
            reader.readAsDataURL(file);

            // on reader load somthing...
            reader.onload = () => {
                // Make a fileInfo Object
                console.log("Called", reader);
                baseURL = reader.result;
                this.setState({
                    imagedata: baseURL,
                })
                console.log(baseURL);
                resolve(baseURL);
            };
            console.log(fileInfo);
        });
    };




    registerProduct(fashionItem) {
        API_FASHION.insertProduct(fashionItem, (result, status, error) => {

            if (result !== null && (status === 200 || status === 201)) {

                console.log("Successfully inserted order " + result);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        })
    }


    handleName = (e) => {
        this.setState({ name: e.target.value });

    }

    handlePrice = (e) => {
        this.setState({ price: e.target.value });

    }

    handleStock = (e) => {
        this.setState({ stock: e.target.value });

    }

    handleGender = (e) => {
        this.setState({ gender: e.target.value });

    }
    handleType = (e) => {
        this.setState({ type: e.target.value });

    }

    handleBrand = (e) => {
        this.setState({ brand: e.target.value });

    }

    handleSize = (e) => {
        this.setState({ size: e.target.value });

    }

    handleColor = (e) => {
        this.setState({ color: e.target.value });

    }




    addFormData(evt) {




        console.log(this.state.imagedata);

        var fashionItem = {}
        fashionItem.name = this.state.name;
        fashionItem.price = this.state.price;
        fashionItem.productImage = this.state.imagedata;
        fashionItem.stock = this.state.stock;
        fashionItem.category = "fashion";
        fashionItem.gender = this.state.gender;
        fashionItem.type = this.state.type;
        fashionItem.brand = this.state.brand;
        fashionItem.size = this.state.size;
        fashionItem.color = this.state.color;
        console.log(fashionItem);
        this.registerProduct(fashionItem);
        Swal.fire({
            title: 'Produsul a fost adaugat cu succes',
            text: "Echipa AtoZ",
            type: 'success',

        });


    }


    render() {

        var user = JSON.parse(localStorage.getItem('user'));
        if (user.role == 'admin') {
            return (



                <div >

                    <header>
                        <Navbar bg="light" expand="lg">
                            <Navbar.Brand class="nav-brand" href="/admin">A to Z</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">



                                    <NavDropdown title="Products" id="basic-nav-dropdown">
                                        <NavDropdown.Item href="/admin/fashion"> <i>Fashion</i></NavDropdown.Item>
                                        <NavDropdown.Item href="/admin/smartPhone"><i>SmartPhones</i></NavDropdown.Item>



                                    </NavDropdown>


                                </Nav>











                            </Navbar.Collapse>

                        </Navbar>
                    </header >
                    <div class="container main-container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 className="text-center mt-5 mb-5">AtoZ</h1>
                                <h1 className="text-center mt-5 mb-5">Introduceti datele produsului pe care doriti sa il postati</h1>
                                <form className="mt-5 mb-5">
                                    <div className="form-group">
                                        <input type="text" className="form-control" id="productname" placeholder="Numele Produsului " value={this.state.name} onChange={this.handleName} />
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control" id="price" placeholder="Pretul produsului " value={this.state.price} onChange={this.handlePrice} />
                                    </div>
                                    <div className="form-group">
                                        <label for="image">Product Image:</label>
                                        <input onChange={(e) => this.handleChange(e.target.files)} type="file" className="form-control" id="image" ref="productimage" />
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control" id="stock" placeholder="Stocul initial al produsului " value={this.state.stock} onChange={this.handleStock} />
                                    </div>




                                    <div className="form-group">
                                        <input type="text" className="form-control" id="gender" placeholder="M/F " value={this.state.gender} onChange={this.handleGender} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="type" placeholder="Tipul produsului(ex.Tricou) " value={this.state.type} onChange={this.handleType} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="brand" placeholder="Brandul Produsului" value={this.state.brand} onChange={this.handleBrand} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="size" placeholder="Marimea produsului" value={this.state.size} onChange={this.handleSize} />
                                    </div>

                                    <div className="form-group">
                                        <input type="text" className="form-control" id="color" placeholder="Culoarea Produsului" value={this.state.color} onChange={this.handleColor} />
                                    </div>





                                    <button type="submit" className="btn btn-primary" onClick={this.addFormData}>Submit</button>
                                </form>






                            </div>
                        </div >
                    </div>
                </div>
            );
        } else {
            return (
                <h1>ACCES RESTRICTIONAT<a href="/">404 ERROR PAGE</a></h1>

            );
        }
    }
} export default FashionInsert;