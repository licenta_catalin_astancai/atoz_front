import React, { Component } from "react";
import { Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import NavigationBar from '../navigation-bar';
import FooterTemplate from "../footer";
import * as API_CUSTOMER from './profileApi';
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";
class Profile extends Component {
  state = {
    customerItem: null,
    id: null,
    isLoaded: false,
    errorStatus: 0,
    error: null
  };
  async componentDidMount() {
    this.fetchProfile();

  }
  fetchProfile() {
    const { user: currentUser } = this.props;
    API_CUSTOMER.getCustomerById(currentUser.id, (result, status, err) => {

      if (result !== null && status === 200) {
        this.setState({
          customerItem: result,
          isLoaded: true
        });
        console.log(this.state.customerItem);


      } else {
        this.setState(({
          errorStatus: status,
          error: err
        }));
      }
    });
  }

  displayCustomer() {
    if (this.state.customerItem != null) {
      return (
        <div class="col-md-11 mt-1">







          <div >

            <p class="newarrival text-center"></p>
            <h1>Name: <b>{this.state.customerItem.nume}</b></h1>
            <h2>Street: {this.state.customerItem.adresa}</h2>
            <h2>Telefon: {this.state.customerItem.telefon}</h2>
            <h2>Email: {this.state.customerItem.username}</h2>
            <h2>Payment: Cash</h2>
            <Button variant="primary" >Update Account</Button>
          </div>
        </div>





      )
    }
  }


  render() {
    const { user: currentUser } = this.props;
    var user = JSON.parse(localStorage.getItem('user'));
    if (!currentUser) {
      return <Redirect to="/login" />;
    }
    if (user.role != 'admin') {
      return (

        <div >
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
          <header >

            <NavigationBar />

          </header>

          <body>
            <div>

              {this.displayCustomer()}

            </div>

          </body>



          <div class="fixed-bottom">
            <FooterTemplate />
          </div>
        </div>
      );
    }
    else {
      return (

        <div >
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
          <header >

            <NavigationBar />

          </header>

          <body>
            <div>

              <div >

                <p class="newarrival text-center"></p>
                <h1>Name: ADMIN</h1>
                <h2>Role: ADMIN</h2>

                <Link to={`/admin`} className="btn btn-primary">Take Tasks</Link>
                <h1></h1>
                <Link to={`/admin/statistic`} className="btn btn-primary">Vezi numarul comenzilor din fiecare luna</Link>
                <h1></h1>
                <Link to={`/admin/turnover`} className="btn btn-primary">Vezi incasarile din fiecare luna</Link>
          
              
              </div>

            </div>

          </body>



          <div class="fixed-bottom">
            <FooterTemplate />
          </div>
        </div>
      );
    }
  }


}
  

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(Profile);