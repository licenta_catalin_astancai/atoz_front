import React, { Component } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';

class Search extends React.Component {

    documentData;

    state = {
        val: ""
      };

    
     
    render() {
        this.documentData = JSON.parse(localStorage.getItem('allEntries'));
        return (

            
            <Form.Group className="form-inline">
                <Form.Control className="from-control"
                   
                  
                    placeholder="search"
                    value={this.state.val}
                  onChange={e => this.setState({ val: e.target.value })}
                    type="search"
                />
               <Link to={`/products/${this.state.val}`} className="btn btn-primary">Search</Link>
                
            </Form.Group>
        );
    }

} export default Search;