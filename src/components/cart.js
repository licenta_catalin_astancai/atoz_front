import React from 'react';
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import NavigationBar from './../navigation-bar';

import Card from 'react-bootstrap/Card';
import moment from "moment";
import * as API_PROUCT from './products/productApi';
import * as API_ORDER from './products/orderAPI';
class Cart extends React.Component {

    documentData;
    constructor(props) {
        super(props);
        this.deleteCart = this.deleteCart.bind(this);
        this.takeOrder = this.takeOrder.bind(this);
        //  this.displayTotal=this.displayTotal(this);
        this.reloadHandler = this.props.reloadHandler;

    }
    state = {
        errorStatus: 0,
        error: null,
        totalPriceProduct: null,

    };




    displayCards() {



        if (this.documentData != null) {
            return (
                this.documentData.map((el, i) => {

                    return (

                        <div class="col-md-2 mt-5">
                            <Card key={i} border="secondary" style={{ width: '18rem' }}>
                                <Card.Img key={i} variant="top" src={`${el.image}`} />
                                <Card.Body key={i}>
                                    <Card.Title key={i}>{el.name}</Card.Title>

                                    <Link to={`/${el.category}/${el.id}`} className="btn btn-primary">ViewProduct</Link>
                                    <Card.Text key={i}> </Card.Text>
                                    <Card.Text key={i}>{el.category} </Card.Text>
                                    <Card.Text key={i}>Quantity {el.q} </Card.Text>
                                    <Card.Text key={i}>Unit price {el.price} $</Card.Text>
                                    <Card.Text key={i}>SubTotal <b>{el.price * el.q}$</b></Card.Text>






                                </Card.Body>
                            </Card>
                        </div>

                    )

                })

            )
        }
    }


    displayTotal() {
        if (this.documentData != null) {
            let max = this.documentData.length;
            for (var i = 0; i < max; i++) {
                this.state.totalPriceProduct += this.documentData[i].price * this.documentData[i].q;
            }
            console.log(this.state.totalPriceProduct);
        }
        if (this.state.totalPriceProduct != null) {
            return (
                <div>
                    <h1><b> Total: {this.state.totalPriceProduct.toFixed(3)} $</b></h1>
                </div>
            )
        } else {
            return (
                <div>
                    <h1><b> Total: 0 $</b></h1>
                </div>
            )
        }

    }

    deleteCart() {
        localStorage.removeItem('allEntries');
        window.location.reload(false);
    }

    registerOrder(order) {
        API_ORDER.insertOrder(order, (result, status, error) => {

            if (result !== null && (status === 200 || status === 201)) {

                console.log("Successfully inserted order " + result);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        })
    }

    takeOrder() {

        var user = JSON.parse(localStorage.getItem('user'));
        if (this.documentData != null && user !=null) {
            var order = {}
            
            var current = new Date();

            var customerID = {};
            customerID.id = user.id;
            order.customerID = customerID

            var date = moment(current).format('YYYY-MM-DD');
            order.date = date;
            order.productList = [];
            order.unitPriceList = [];
            order.quantity = [];
            this.documentData.forEach(e => {
                var id = {};
                id.id = e.id + '';
                order.productList.push(id);
                order.unitPriceList.push(e.price);
                order.quantity.push(e.q);
            })
            console.log(order);
            this.registerOrder(order);
            localStorage.removeItem('allEntries');
            window.location.reload(false);
            alert('Comanda a fost plasta de ' + user.username + ' in valoarea de ' + this.state.totalPriceProduct + ' $ ');
        }
        else
        {
            alert('trebuie sa aveti cont pentru a face comanda');
               
            window.location.reload(false);
               
            }
    }

    render() {
        this.documentData = JSON.parse(localStorage.getItem('allEntries'));
        const { user: currentUser } = this.props;
        console.log(this.state.prodItems);
        return (

            <div >

                <header>
                    <NavigationBar />
                </header>
                <body>
                    <h1>Cosul dumneavoastra de cumparaturi</h1>
                   

                    <div class="card-group">
                    {this.displayCards()}
                    </div>
                    {this.displayTotal()}

                    <Button variant="danger" onClick={this.deleteCart}>Delete Cart</Button>
                    <Button variant="primary" onClick={this.takeOrder}>Place Order</Button>
                    <div class="fixed-bottom">
                   
                   
                   
                    </div>


                </body>



            </div>
        );
    }
}

export default Cart;