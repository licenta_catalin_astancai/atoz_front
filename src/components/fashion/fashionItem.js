import React from 'react';
import './cardStyles.css';

import NavigationBar from '../../navigation-bar';
import FooterTemplate from '../../footer';
import Card from 'react-bootstrap/Card';

import * as API_FASHION from './fashionApi';
import { Link } from 'react-router-dom';
class Fashion extends React.Component {




    state = {
        fashionItems: [],
        isLoaded: false,
        errorStatus: 0,
        error: null
    };



    async componentDidMount() {
        this.fetchFashionItems();
    }


    fetchFashionItems() {
        API_FASHION.getFashionProducts((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    fashionItems: result,

                    isLoaded: true
                });
                console.log(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    displayCards() {



        if (this.state.fashionItems != null) {
            return (
                this.state.fashionItems.map((el, i) => {

                    return (

                        <div class="col-md-2 mt-11">
                            <Card key={i} border="secondary" style={{ width: '18rem' }}>
                                <Card.Img key={i} variant="top" src={`${el.productImage}`} />
                                <Card.Body key={i}>
                                    <Card.Title key={i}>{el.name}</Card.Title>
                                    <Card.Text key={i}>{el.gender}</Card.Text>
                                    <Card.Text key={i}>{el.size}</Card.Text>
                                    <Card.Text key={i}>{el.price}</Card.Text>

                                    <Link to={`/fashion/${el.id}`} className="btn btn-primary">ViewProduct</Link>

                                </Card.Body>
                            </Card>
                        </div>

                    )
                })

            )
        }
    }


    render() {
      console.log(this.state.fashionItems.length);
        if (this.state.fashionItems.length >= 7) {
            return (

                <div >

                    <header>
                        <NavigationBar />
                    </header>
                    <body>

                        <div class="card-group">

                            {this.displayCards()}

                        </div>




                    </body>
                    <footer>

                        <FooterTemplate />

                    </footer>
                </div>
            );
        }
        else {
            return (

                <div >

                    <header>
                        <NavigationBar />
                    </header>
                    <body>

                        <div class="card-group">

                            {this.displayCards()}

                        </div>




                    </body>
                    <footer>
                        <div class="fixed-bottom">

                            <FooterTemplate />

                        </div>


                    </footer>
                </div>
            );
        }
    }

}
export default Fashion;