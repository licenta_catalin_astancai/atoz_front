import React from 'react';
import NavigationBar from '../../navigation-bar';
import FooterTemplate from '../../footer';
import * as API_FASHION from './fashionApi';
import { Link } from 'react-router-dom';
class FashionItemDisplay extends React.Component {

    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    handleChange(e) {
        this.state.quantityFinal=e.currentTarget;
      }

      handleInput(e) {
        this.state.quantityFinal=e.currentTarget;
      }
    state = {
        fashionItem: null,

        isLoaded: false,
        errorStatus: 0,
        error: null,

        id: null,
        name: null,
        price: null,
        stock: null,
        category:null,
        image:null,
        q:null,
        quantityFinal: '',
        subTotal:null
    };
    async componentDidMount() {
        this.fetchFashionItems();


    }
    fetchFashionItems() {
        let string = String(window.location.pathname.replace('/fashion/', ''));
        let id_ok = string;
        API_FASHION.getFashionProductById(id_ok, (result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    fashionItem: result,

                    isLoaded: true
                });




            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    addToCart() {

        if (this.state.fashionItem != null) {
        let fashioItemCart={
            id: this.state.fashionItem.id,
            name:this.state.fashionItem.name,
            price:this.state.fashionItem.price,
            category:this.state.fashionItem.category,
            image:this.state.fashionItem.productImage,
            stock:this.state.fashionItem.stock,
            q:Number(this.state.quantityFinal.value),
            
        };
          
           
            
        alert('Produs adaugat cu succes in cos');
            var existingEntries = JSON.parse(localStorage.getItem("allEntries"));
            if (existingEntries == null) existingEntries = [];

            existingEntries.push(fashioItemCart);
            localStorage.setItem("allEntries", JSON.stringify(existingEntries));
        }
    };

    displayProduct() {
        if (this.state.fashionItem != null) {
            return (
                <div class="col-md-11 mt-1">
                    <div class="col-md-7">
                        <p class="newarrival text-center"></p>
                        <h2>{this.state.fashionItem.name}</h2>
                        <p><b>Size {this.state.fashionItem.size}</b></p>
                        <p><b>Price: {this.state.fashionItem.price} $</b></p>
                        <p><b>Gender: {this.state.fashionItem.gender}</b></p>
                        <p><b>Brand:</b>{this.state.fashionItem.brand}</p>
                        <p><b>Quantity</b></p>
                    </div>
                </div>
            )
        }
    }

    displayProductImage() {
        if (this.state.fashionItem != null) {
            return (<img src={`${this.state.fashionItem.productImage}`} class="rounded float-right" />
            )
        }
    }



    render() {
        return (

            <div >

                <header>
                    <NavigationBar />
                </header>

                <body>

                    <div class="card-group">


                        <form action>
                            {this.displayProduct()}
                            <label class="newarrival text-center" for="quantityFinal"><b></b></label>
                            <input type="number" id="quantityFinal" name="quantityFinal" min="1" max="50"  onChange={this.handleChange} onInput={this.handleInput}/>
                            <input type="submit" value="Add to Cart" onClick={this.addToCart}/>

                            {this.displayProductImage()}
                        </form>



                    </div>




                </body>

            </div>

        );
    }

}
export default FashionItemDisplay;
