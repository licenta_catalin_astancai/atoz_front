import {HOST} from'../../commons/host';
import RestApiClient from "../../commons/api/rest-client.js";

const endpoint={
    get_fashion:"/fashion",
   delte_action:"/delete/",
   update_action:"/update/"
};


function getFashionProducts(callback){
    let request = new Request(HOST.backend_api + endpoint.get_fashion, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getFashionProductById(params,callback){
    let request = new Request(HOST.backend_api + endpoint.get_fashion+"/"+ params, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback); 
}

function deleteFashion(params, callback){
    let user = localStorage.getItem('token');
    let request = new Request(HOST.backend_api+endpoint.get_fashion+ endpoint.delte_action + params, {
        method: 'DELETE',
        headers : {
            'Authorization': 'Bearer ' + user,
          
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function insertProduct(product, callback) {
    
    let user = localStorage.getItem('token');
    console.log(JSON.stringify(product));
    let request = new Request(HOST.backend_api + endpoint.get_fashion , {
        method: 'POST',
        headers : {
            'Authorization': 'Bearer ' + user,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },

       body: JSON.stringify(product)
    });
    RestApiClient.performRequest(request, callback);
}

function updateFashion(fashionItem,id,callback){
    let user = localStorage.getItem('token');
    let request = new Request(HOST.backend_api +endpoint.get_fashion +endpoint.update_action+id , {
        method: 'PUT',
        headers : {
            'Authorization': 'Bearer ' + user,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(fashionItem)
    });


    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export{
    getFashionProducts,
    getFashionProductById,
    insertProduct,
    deleteFashion,
    updateFashion
};