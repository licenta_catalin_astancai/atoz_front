import React from 'react';
import {Bar} from 'react-chartjs-2';
import * as API_STATISTIC from './statisticsAPI';
import NavigationBar from '../../navigation-bar';
import FooterTemplate from "../../footer";
 


export default class Statistic extends React.Component {
    
    state = {
        fashionItems: [],
        isLoaded: false,
        errorStatus: 0,
        error: null
    };


    async componentDidMount() {
        this.fetchFashionItems();
    }
    
    
    fetchFashionItems() {
        API_STATISTIC.getStatistic((result, status, err) => {
    
            if (result !== null && status === 200) {
                this.setState({
                    fashionItems: result,
                   
                    isLoaded: true
                });
                console.log(result);
                console.log(this.state.fashionItems);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


reloadPage(){
    window.location.reload(false);
}



  render() {

    const date = {
        labels: ['January', 'February', 'March',
                 'April', 'May','June','July','August','September','Octomber','November','December'],
        datasets: [
          {
            label: 'Orders',
            backgroundColor: 'rgba(75,120,192,1)',
            borderColor: 'rgba(0,0,0,1)',
            borderWidth: 1,
            data: this.state.fashionItems
          }
        ]
      }

    return (
      <div>
           <header>
                    <NavigationBar />
                </header>
    <body>
    <button onClick={this.reloadPage}>Click to reload!</button>
        <Bar
          data={date}
         
          options={{

            animationEnabled: true,
            title:{
              display:true,
              text:'Numarul Comenzilor pe luni',
              fontSize:30
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />


    </body>
       
            <FooterTemplate />
         
      </div>
    );
  }
}