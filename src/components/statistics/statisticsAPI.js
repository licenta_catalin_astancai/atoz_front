import {HOST} from'../../commons/host';
import RestApiClient from "../../commons/api/rest-client.js";

const endpoint={
    get_statistic:"/order/data",
    get_turnover:"/orderdetails/turnover"
  
};


function getStatistic(callback){
    let request = new Request(HOST.backend_api + endpoint.get_statistic, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getTurnover(callback){
    let request = new Request(HOST.backend_api + endpoint.get_turnover, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export{
    getStatistic,
    getTurnover
};